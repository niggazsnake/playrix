# -*- coding: utf-8 -*-
import datetime
import pandas as pd
from pandas import read_csv
import csv

params = {"id": 2, "date_start": "2016-05-02", "date_end": "2016-05-10", "file_installs": "in/installs.csv",
          "file_purchases": "in/purchases.csv", "file_result": "out/result.csv"}

installs = read_csv(params["file_installs"])
purchases = read_csv(params["file_purchases"])

installs = installs[(installs['created'] >= params["date_start"]) & (installs['created'] <= params["date_end"])
                    & (installs['mobile_app'] == params["id"])]

purchases = purchases[(purchases['install_date'] >= params["date_start"]) & (purchases['install_date'] <= params["date_end"])
                      & (purchases['mobile_app'] == params["id"])]

purchases['created_new'] = pd.to_datetime(purchases['created'])
purchases['install_date_new'] = pd.to_datetime(purchases['install_date'])
purchases['days'] = (purchases['created_new'] - purchases['install_date_new']).apply(lambda x: x.days)

purchases = purchases.drop('created_new', 1)
purchases = purchases.drop('install_date_new', 1)
purchases = purchases.drop('created', 1)
purchases = purchases.drop('install_date', 1)


purchases_days_sum = purchases.pivot_table(values=['revenue'], index=['country'], columns=['days'],
                                           aggfunc='sum', margins=True)

installs_count = installs.groupby(["country"]).count()
installs_count = installs_count.drop('mobile_app', 1)

res_merge = pd.concat([installs_count, purchases_days_sum], axis=1)
res_merge = res_merge.sort_values(by=['created'], ascending=False)



with open(params['file_result'], 'w') as csvfile:
    fieldnames = ['country', 'installs', "RPI1", "RPI2", "RPI3", "RPI4", "RPI5", "RPI6", "RPI7", "RPI8", "RPI9",
                  "RPI10"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for row in res_merge.itertuples():
        if row[0] != 'All':
            out_dict = {}
            out_dict['country'] = row[0]
            out_dict['installs'] = int(row[1])
            sum = 0
            for day in xrange(2, 12):
                sum += row[day]
                value = round(sum / out_dict['installs'], 2)
                key = "RPI%s" % (day - 1)
                out_dict[key] = value

            writer.writerow(out_dict)

