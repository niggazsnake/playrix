# -*- coding: utf-8 -*-
import datetime
import csv


params = {"id": "2", "date_start": "2016-05-02", "date_end": "2016-05-09", "file_installs": "in/installs.csv",
          "file_purchases": "in/purchases.csv", "file_result": "out/result.csv"}

year_start, mounth_start, day_start = params["date_start"].split("-")
year_end, mounth_end, day_end = params["date_end"].split("-")

installs = open(params["file_installs"], "r")
purchases = open(params["file_purchases"], "r")

data_installs = {}
data_rpi = {}


def check_data_to_params(id, str_date):
    if str_date:
        year, mounth, day = str_date.split('-')

        if id == params["id"] and year >= year_start and year <= year_end \
                and mounth >= mounth_start and mounth <= mounth_end \
                and day >= day_start and day <= day_end:
            return True
        else:
            return False


def exec_installs():
    for line in installs:
        if line:
            date, id, country = line.rstrip().split(',')
            if date.find('-') > 0:
                date = date.split(" ")[0]
                if check_data_to_params(id, date):
                    if country in data_installs:
                        data_installs[country] += 1
                    else:
                        data_installs[country] = 1

def str_to_date(str):
    return datetime.datetime.strptime(str, "%Y-%m-%d %H:%M:%S").date()

def exec_purchaces():
    for line in purchases:
        if line:
            date_purchase, id, country, date_install, revenue = line.rstrip().split(',')
            if date_install.find('-') > 0:
                if check_data_to_params(id, date_install):
                    date_subtract = str_to_date(date_purchase) - str_to_date(date_install)
                    days = date_subtract.days
                    if days >= 0:
                        if country not in data_rpi:
                            data_rpi[country] = {}

                        if days in data_rpi[country]:
                            data_rpi[country][days] += float(revenue)
                        else:
                            data_rpi[country][days] = float(revenue)
                    else:
                        print "error. Days"


def write_to_csv():
    with open(params['file_result'], 'w') as csvfile:
        fieldnames = ['country', 'installs', "RPI1", "RPI2", "RPI3", "RPI4", "RPI5", "RPI6", "RPI7", "RPI8", "RPI9",
                      "RPI10"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for country in sorted(data_installs, key=lambda x: data_installs[x], reverse=True):
            total_installs = data_installs[country]

            rpi_country = data_rpi[country]

            summ_curr_rpi = 0
            out_dict = {}
            out_dict['country'] = country
            out_dict['installs'] = total_installs
            for day in xrange(0, 10):
                if day in rpi_country:
                    summ_curr_rpi += rpi_country[day]
                    curr_rpi = round(summ_curr_rpi / total_installs, 2)
                    key = "RPI%s" % (day + 1)
                    out_dict[key] = curr_rpi
                else:
                    print "error finding"
            writer.writerow(out_dict)

print "processing file: installs.csv"
exec_installs()
print "processing file: purchaces.csv"
exec_purchaces()
print data_rpi
print "making output results"
write_to_csv()